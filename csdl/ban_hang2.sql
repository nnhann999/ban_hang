-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2023 at 12:42 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ban_hang2`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `hinh` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `rong` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cao` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `hinh`, `rong`, `cao`) VALUES
(1, 'banner_2.png', '990px', '150px');

-- --------------------------------------------------------

--
-- Table structure for table `hoa_don`
--

CREATE TABLE `hoa_don` (
  `id` int(11) NOT NULL,
  `ten_nguoi_mua` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dia_chi` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dien_thoai` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hang_duoc_mua` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loai`
--

CREATE TABLE `loai` (
  `id` int(11) NOT NULL,
  `ten` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loai`
--

INSERT INTO `loai` (`id`, `ten`) VALUES
(1, 'Đầm Xòe'),
(2, 'Đầm Công Sở'),
(3, 'Đầm Suông'),
(4, 'Đầm Bầu'),
(5, 'Đầm Body'),
(6, 'Đầm Ngủ');

-- --------------------------------------------------------

--
-- Table structure for table `menu_ngang`
--

CREATE TABLE `menu_ngang` (
  `id` int(11) NOT NULL,
  `ten` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `loai_menu` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_ngang`
--

INSERT INTO `menu_ngang` (`id`, `ten`, `noi_dung`, `loai_menu`) VALUES
(1, 'Giới thiệu', '<p>Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /> Nội dung phần giới thiệu <br /><br /></p>', ''),
(2, 'Sản phẩm', '', 'san_pham'),
(3, 'Hướng dẫn  ', 'Nội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>\r\nNội dung hướng dẫn mua hàng <br><br>', ''),
(4, 'Cách mua hàng', 'Nội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>\r\nNội dung cách mua hàng <br><br>', ''),
(5, 'Liên hệ', 'Nội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>\r\nNội dung liên hệ<br><br>', '');

-- --------------------------------------------------------

--
-- Table structure for table `san_pham`
--

CREATE TABLE `san_pham` (
  `id` int(11) NOT NULL,
  `ten` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `noi_bat` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hinh_anh` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gia` int(255) NOT NULL,
  `ma_loai` int(255) NOT NULL,
  `trang_chu` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sap_xep_trang_chu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `maloai` varchar(13) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `san_pham`
--

INSERT INTO `san_pham` (`id`, `ten`, `noi_dung`, `noi_bat`, `hinh_anh`, `gia`, `ma_loai`, `trang_chu`, `sap_xep_trang_chu`, `maloai`) VALUES
(1, 'Đầm maxi trắng có yếm', '<p>Đầm maxi trắng có yếm</p>', 'co', '1.jpg', 55000, 1, '', '0', ''),
(2, 'Đầm chân vấy xòe', '<p>Đầm chân vấy xòe</p>', '', '3.jpg', 82000, 1, 'co', '7', 'DX'),
(3, 'Đầm dự tiệc ', 'Đầm dự tiệc màu trắng', '', '1_2.jpg', 86000, 1, '', '0', 'DX'),
(4, 'Đầm dự tiệc ', 'Vấy đầm dự tiệc màu trắng', '', '1_3.jpg', 97000, 1, 'co', '3', 'DX'),
(5, 'Đầm trắng suông ', 'Đầm trắng suông Hàn Quốc đẹp', '', '1_4.jpg', 42000, 3, '', '0', 'DS'),
(6, 'đầm dự tiệc chữ V', '<p>Vấy đầm dự tiệc chữ V</p>', '', '1_5.jpg', 100000, 1, 'co', '0', ''),
(7, 'Đầm maxi dự tiệc ', 'Đầm maxi dự tiệc màu trắng', '', '1_6.jpg', 120000, 1, 'co', '2', 'DX'),
(8, 'Đầm voan dực tiệc', 'Đầm voan dực tiệc màu trắng', '', '1_7.jpg', 80000, 1, '', '0', 'DX'),
(9, 'Kiểu vấy đầm xòe', 'Kiểu vấy đầm xòe xếp ly', '', '1_8.jpg', 160000, 1, '', '0', 'DX'),
(10, 'Vấy đầm suông trắng', 'Vấy đầm suông trắng', '', '1_9.jpg', 160000, 3, 'co', '7', 'DS'),
(11, 'Kiểu đầm xòe', '<p>Kiểu vấy đầm xòe chữ A</p>', '', '1_10.jpg', 135000, 1, 'co', '0', 'DX'),
(12, 'Kiểu đầm xòe thêu hoa', '<p>Kiểu vấy đầm xòe thêu hoa</p>', '', '1_11.jpg', 55000, 1, 'co', '13', 'DX'),
(13, 'Đầm suông form rộngg', '<p>Đầm suông form rộng</p>', '', '1_12.jpg', 72000, 3, '', '0', 'DS'),
(14, 'Đầm dự tiệc body trang', '<p>Đầm dự tiệc body</p>', '', '1_13.jpg', 78000, 5, 'co', '11', 'DBD'),
(15, 'Kiểu vấy đầm xòe', '<p>kiểu vấy đầm xòe </p>', '', '1_14.jpg', 123000, 1, 'co', '0', 'DX'),
(16, 'đầm xòe đi biển ', 'Kiểu vấy đầm xòe đi biển ', 'co', '1_15.jpg', 125000, 1, 'co', '5', 'DX'),
(17, 'Đầm dự tiệc trắngg', 'Đầm dự tiệc trắng', '', '1_16.jpg', 99000, 1, '', '0', 'DX'),
(18, 'kiểu vấy đầm xòe', '<p>kiểu vấy đầm xòe</p>', '', '1_17.jpg', 145000, 3, 'co', '1', 'DS'),
(19, 'Đầm xòe công sở', 'Đầm xòe công sở', '', '1_18.jpg', 145000, 2, '', '0', 'DCS'),
(20, 'Đầm suông đen Hàn Quốc', 'Đầm suông đen Hàn Quốc', '', '1_19.jpg', 170000, 3, '', '0', 'DS'),
(21, 'Đầm dự tiệc chân vấy đuôi cá', 'Đầm dự tiệc chân vấy đuôi cá', '', '1_20.jpg', 85000, 1, '', '0', 'DX'),
(22, 'Đầm suông sọc trắng đen', '<p>Đầm suông sọc trắng đen</p>', '', '3_2.jpg', 30000, 3, 'co', '8', 'DS'),
(23, 'Đầm sòe liền thân', 'Đầm sòe liền thân', '', '3_3.jpg', 40000, 1, '', '6', 'DX'),
(24, 'Đầm liền thân ngắn', 'Đầm liền thân ngắn', '', '3_4.jpg', 50000, 2, '', '0', 'DCS'),
(25, 'Vấy đầm liền thân ', 'Vấy đầm liền thân ', '', '3_5.jpg', 60000, 1, '', '9', 'DX'),
(26, 'Đầm body đen', '<p>Đầm dự tiệc body </p>', '', '3_6.jpg', 70000, 5, 'co', '12', 'DBD'),
(27, 'Đầm công sở lệch vai', 'Đầm dự tiệc lệch vai', '', '3_7.jpg', 80000, 2, '', '0', 'DCS'),
(28, 'Đầm công sở body', '<p>Đầm công sở body</p>', '', '3_8.jpg', 90000, 5, '', '8', 'DBD'),
(29, 'Đầm bầu công sở', 'Đầm bầu công sở', '', '3_9.jpg', 100000, 4, '', '0', 'DB'),
(30, 'Đầm đen công sở', 'Đầm đen công sở', '', '3_10.jpg', 110000, 2, '', '7', 'DCS'),
(31, 'Đầm suông ngắn', '<p>Đầm suông ngắn</p>', '', '3_11.jpg', 120000, 3, 'co', '9', 'DS'),
(32, 'Vấy đầm bầu', 'Vấy đầm bầu', '', '3_12.jpg', 50000, 4, '', '12', 'DB'),
(33, 'Đầm công sở', '<p>Đầm công sở</p>', '', '3_13.jpg', 60000, 1, '', '1', 'DX'),
(34, 'Vấy đầm bầu den', 'Vấy đầm bầu', '', '3_14.jpg', 70000, 4, '', '11', 'DB'),
(35, 'Đầm suông chữ A', '<p>Đầm suông chữ A sát nách</p>', '', '3_15.jpg', 80000, 3, '', '0', 'DS'),
(36, 'Đầm công sở', 'Vấy đầm công sở', '', '3_16.jpg', 90000, 2, '', '16', 'DCS'),
(37, 'đầm xòe phối lưới', 'Kiểu vấy đầm xòe phối lưới', '', '3_17.jpg', 170000, 1, '', '15', 'DX'),
(38, 'Đầm công sở lệch vai', 'Đầm công sở lệch vai', '', '3_18.jpg', 180000, 2, '', '0', 'DCS'),
(39, 'Đầm xòe 2 dây', '<p>Đầm xòe 2 dây</p>', 'co', '3_19.jpg', 190000, 1, '', '0', 'DX'),
(40, 'Đầm xòe dự tiệc đen', '<p>Đầm xòe dự tiệc đen</p>', '', '3_20.jpg', 200000, 1, 'co', '2', 'DX'),
(41, 'uoiuo', '<p>uy yui yu</p>', 'co', '244645259_1490089191376094_1169663161325750616_n.jpg', 3445566, 3, 'co', '10', '');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE `slideshow` (
  `id` int(11) NOT NULL,
  `hinh` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lien_ket` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `hinh`, `lien_ket`) VALUES
(1, 'a_1.png', '#'),
(2, 'a_2.png', '#'),
(3, 'a_3.png', '#'),
(4, 'a_4.png', '#');

-- --------------------------------------------------------

--
-- Table structure for table `thong_tin_quan_tri`
--

CREATE TABLE `thong_tin_quan_tri` (
  `id` int(11) NOT NULL,
  `ky_danh` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thong_tin_quan_tri`
--

INSERT INTO `thong_tin_quan_tri` (`id`, `ky_danh`, `mat_khau`) VALUES
(1, 'admin', 'e19d5cd5af0378da05f63f891c7467af');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hoa_don`
--
ALTER TABLE `hoa_don`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loai`
--
ALTER TABLE `loai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_ngang`
--
ALTER TABLE `menu_ngang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `san_pham`
--
ALTER TABLE `san_pham`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loai` (`maloai`);

--
-- Indexes for table `slideshow`
--
ALTER TABLE `slideshow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `thong_tin_quan_tri`
--
ALTER TABLE `thong_tin_quan_tri`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hoa_don`
--
ALTER TABLE `hoa_don`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loai`
--
ALTER TABLE `loai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menu_ngang`
--
ALTER TABLE `menu_ngang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `san_pham`
--
ALTER TABLE `san_pham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `slideshow`
--
ALTER TABLE `slideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `thong_tin_quan_tri`
--
ALTER TABLE `thong_tin_quan_tri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
